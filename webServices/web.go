package webServices

import (
	"fmt"
	"os"

	"bitbucket.org/TIKI-Institut/ai-common-go/v2/dsp/web"
	"github.com/gin-gonic/gin"
)

func RegisterWebServices(webEngine *web.WebEngine) {
	webEngine.RegisterEndpointPath("GET", "/test_open", nil, func(context *gin.Context) {
		context.String(200, "This is an open endpoint from the main with no login required")
	})

	webEngine.RegisterEndpointPath("GET", "/test_secured", []string{}, func(context *gin.Context) {
		context.String(200, "This endpoint from main need a login")
	})

	webEngine.RegisterEndpointPath("GET", "/test_secured_with_roles", []string{"capability-1"}, func(context *gin.Context) {
		context.String(200, "This endpoint from main need a login and a role")
	})

	webEngine.RegisterEndpointPath("GET", "/resource_limits", nil, func(context *gin.Context) {
		cpuLimit, ok := os.LookupEnv("CPU_LIMIT")
		if !ok {
			cpuLimit = "unavailable"
		}
		memoryLimit, ok := os.LookupEnv("MEMORY_LIMIT")
		if !ok {
			memoryLimit = "unavailable"
		}
		context.String(200, fmt.Sprintf("cpu limit: %s m, memory limit: %s Mi", cpuLimit, memoryLimit))
	})

}
