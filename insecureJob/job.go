package insecureJob

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/dsp"
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/logging"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
)

func action(*cobra.Command, []string) error {
	logging.SetupLogger("LOG_LEVEL")
	log.Info().Msg("Hello World (from insecure-job)")
	return nil
}

func RegisterJob(cmd *cobra.Command) {

	subCmd := &cobra.Command{
		Use:   "insecure-job",
		Short: "basic insecure job",
		Long:  "",
		Args:  cobra.ExactArgs(0),
		RunE:  action,
	}
	dsp.AddSubCommand(cmd, subCmd, nil)

}
