package insecureJobWithArgs

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/dsp"
	"fmt"
	"github.com/spf13/cobra"
	"strconv"
)

func action(_ *cobra.Command, args []string) error {

	name := args[0]
	count, err := strconv.Atoi(args[1])

	if err != nil {
		return err
	}

	for i := 0; i < count; i++ {
		fmt.Println(fmt.Sprintf("Hello %s! (%d)", name, i))
	}

	return nil
}

func RegisterJob(cmd *cobra.Command) {

	subCmd := &cobra.Command{
		Use:   "insecure-job-with-cli-args [NAME] [COUNT]",
		Short: "basic insecure job with arguments",
		Long:  "",
		Args:  cobra.ExactArgs(2),
		RunE:  action,
	}

	dsp.AddSubCommand(cmd, subCmd, nil)
}
