package secureJob

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/dsp"
	"fmt"
	"github.com/spf13/cobra"
)

func secureJobAction(_ *cobra.Command, _ []string) error {
	fmt.Println("Hello World (from secure-job)")
	return nil
}

func RegisterJob(cmd *cobra.Command) {

	subCmd := &cobra.Command{
		Use:   "secure-job",
		Short: "basic secure job",
		Long:  "",
		Args:  cobra.ExactArgs(0),
		RunE:  secureJobAction,
	}

	dsp.AddSubCommand(cmd, subCmd, []string{"capability-2"})
}
