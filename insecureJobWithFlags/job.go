package insecureJobWithFlags

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/dsp"
	"fmt"
	"github.com/spf13/cobra"
)

var name string
var count int

func action(_ *cobra.Command, _ []string) error {
	for i := 0; i < count; i++ {
		fmt.Println(fmt.Sprintf("Hello %s! (%d)", name, i))
	}
	return nil
}

func RegisterJob(cmd *cobra.Command) {

	subCmd := &cobra.Command{
		Use:   "insecure-job-with-cli-flags",
		Short: "basic insecure job with flag arguments",
		Long:  "",
		Args:  cobra.ExactArgs(0),
		RunE:  action,
	}

	subCmd.PersistentFlags().StringVar(&name, "name", "", "The person to greet.")
	subCmd.PersistentFlags().IntVar(&count, "count", 1, "Number of greetings.")

	dsp.AddSubCommand(cmd, subCmd, nil)
}
