module bitbucket.org/TIKI-Institut/ai-example-go

go 1.16

require (
	bitbucket.org/TIKI-Institut/ai-common-go/v2 v2.2.10
	github.com/coreos/go-oidc v2.1.0+incompatible // indirect
	github.com/gin-contrib/sse v0.1.0 // indirect
	github.com/gin-gonic/gin v1.3.0
	github.com/rs/zerolog v1.20.0
	github.com/spf13/cobra v0.0.3
	gopkg.in/square/go-jose.v2 v2.3.1 // indirect
)

// todo cobra introduced support for context.Context but left the feature to update the context used
// todo close when https://github.com/spf13/cobra/pull/1118 gets merged
replace github.com/spf13/cobra => github.com/seashell/cobra v1.0.1-0.20200519200726-d0e2899afbe2
