package main

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/cli"
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/dsp/web"
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/logging"
	"bitbucket.org/TIKI-Institut/ai-example-go/insecureJob"
	"bitbucket.org/TIKI-Institut/ai-example-go/insecureJobWithArgs"
	"bitbucket.org/TIKI-Institut/ai-example-go/insecureJobWithFlags"
	"bitbucket.org/TIKI-Institut/ai-example-go/secureJob"
	"bitbucket.org/TIKI-Institut/ai-example-go/webServices"
	"context"
	"github.com/rs/zerolog/log"
	"os"
	"syscall"
)

func main() {
	//Set up the global context & the web engine
	logging.SetupLogger("")

	ctx := log.Logger.WithContext(context.Background())
	ctx = cli.NewContextWithSignalListener(ctx, cli.SignalListener(syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL))
	webEngine := web.NewWebEngine(ctx)

	//Register the endpoints of the webservice. No context for registering needed, because
	//the jwt information is injected via the gin context in the web session
	webServices.RegisterWebServices(webEngine)

	//In this case the web.MainCommand, because it can handle jobs and webservices
	mainCmd := web.NewMainCommand(webEngine)

	//Register jobs (context needed because of jwt stuff in the registration code)
	insecureJob.RegisterJob(mainCmd)
	insecureJobWithArgs.RegisterJob(mainCmd)
	insecureJobWithFlags.RegisterJob(mainCmd)
	secureJob.RegisterJob(mainCmd)

	//Now execute the jobs & webservice
	if err := mainCmd.Execute(); err != nil {
		os.Exit(1)
	}
}
